﻿<?php 

$errors = array();

function fieldname_as_text($fieldname)
	{
		$fieldname = str_replace("_", " ", $fieldname);
		$fieldname = ucfirst($fieldname);
		return $fieldname;
	}


// * Presence
// Use trim() so empty spaces don't count
// Use === to avoid false positives
// empty() would consider "0" to be empty

function has_presence($value)
	{
		return isset($value) && $value !== "";
	}

function validate_presences($required_fields)
	{
		global $errors;
		foreach ($required_fields as $field)
			{
				$value = trim($_POST[$field]);
				if (!has_presence($value))
					{
						$errors[$field] = fieldname_as_text($field) . " can't be blank";
					}
			}
	}
	
// * String Length
// max length

function has_max_length($value, $max)
	{
		return strlen($value) <= $max;
	}

function validate_max_lengths($fields_with_max_lengths)
{
	global $errors;
	// Using an associate array
	foreach($fields_with_max_lengths as $field => $max)
		{
			$value = trim($_POST[$field]);
			if(!has_max_length($value, $max))
				{
					$errors[$field] = fieldname_as_text($field) . " is too long";
				}
		}

}
	
// * Inclusion in a set_error_handler

function has_inclusion_in($value, $set)
	{
		in_array($value, $set);
	}

	



?>